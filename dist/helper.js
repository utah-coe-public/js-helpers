var HELPERS = (function(){

    var helper = {};

    helper.globals = {
        mobile_width: 768,
    };

    return helper;
}());

HELPERS.clone = function(obj){
    return (JSON.parse(JSON.stringify(obj)));
}

HELPERS.ul2Csv = function(element){

    var array = [];
    element.find('li').each(function(key,value){
        array.push($(value).text());
    });    
    return array.join(', ');
}

HELPERS.logVueObject = function(obj){
    console.log(JSON.parse(JSON.stringify(obj)));
}

HELPERS.getElementById = function(elements, id){
    var selected_element = null;

    var arrayLength = elements.length;
    for (var i = 0; i < arrayLength; i++) {
        if(elements[i].id == id){
            selected_element = elements[i];
            return selected_element;
        }
        //Do something
    }

    return selected_element;
}

HELPERS.getElementByFieldValue = function(elements, field, value){
    var selected_element = null;

    var arrayLength = elements.length;
    for (var i = 0; i < arrayLength; i++) {
        if(elements[i][field] == value){
            selected_element = elements[i];
            return selected_element;
        }
        //Do something
    }

    return selected_element;
}

HELPERS.ajaxParseJavascript = function(html){
    var dom = $(html); 
    dom.filter('script').each(function(){
        $.globalEval(this.text || this.textContent || this.innerHTML || '');
    });
}

HELPERS.reloadPage = function(){
    window.location.href="";
};

HELPERS.getFormArray = function($element){
    var id = $element.attr('id');    
    var form = document.getElementById(id);
    var formData = new FormData(form);

    var form_data = [];
    for(var pair of formData.entries()) {
       form_data[pair[0]] = pair[1]; 
    }

    return form_data;
}

HELPERS.getFormData = function($element){
    var id = $element.attr('id');    
    var form = document.getElementById(id);
    var formData = new FormData(form);

    return formData;    
}

HELPERS.clearForm = function($element){
    var id = $element.attr('id');        
    var form = document.getElementById(id);
    form.reset();
    return true;
}


HELPERS.windowWidth = function(){
    return $(window).width();
}

HELPERS.isMobile = function(){
    return HELPERS.windowWidth() < HELPERS.globals.mobile_width ? true : false;
}

HELPERS.Formatter = (function(){
    var Formatter = {};
    
    Formatter.phonenumber = function(phonenumber){
        var phonenumber_regexp = /^\(?([0-9]{3})\)?[-. ]*([0-9]{3})[-. ]*([0-9]{4})$/;
        return phonenumber.replace(phonenumber_regexp, "$1-$2-$3");
    }

    return Formatter;
}())

HELPERS.Spinner = (function(){

    var Spinner = {
        html: '<div class="showbox">  <div class="loader">    <span style="color: white; text-align:center; position:absolute; top: 40px; left: 30px;"> loading </span> <svg class="circular" viewBox="25 25 50 50">      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>    </svg>  </div></div>',
        dom_element: null
    }

    Spinner.launch = function(){
        if(_.isUndefined(Spinner.dom_element) || _.isNull(Spinner.dom_element)){
            Spinner.dom_element = $(Spinner.html);
            $('body').first().append(Spinner.dom_element);
        }
    }

    Spinner.destroy = function(){
        if(!_.isUndefined(Spinner.dom_element) && !_.isNull(Spinner.dom_element)){
            Spinner.dom_element.remove();
            Spinner.dom_element = null; 
        }
    }

    Spinner.delayedDestroy = function(delay){
        window.setTimeout(function(){
            Spinner.destroy();

        }, delay);
    }

    return Spinner;

}())

HELPERS.Modal = (function(){
    

    var Modal = {fancybox: null};

    ////
    //  Modal window currently using fancybox.
    ////
    Modal.options =  {
            padding: 20,
            margin: 40,
            helpers: {
                overlay : { closeClick: false,
                            closeButton: true,
                        },
                title : {
                    position: 'top',
                    type: 'inside',
                },
            },
            closeBtn: true,             
            autoSize: true,
            width: 600,

        };

    Modal.configureOptions = function(options){

            if(typeof(options) == 'undefined'){
                options = {};
            }

            //padding
            Modal.options.padding = options.padding != undefined ? options.padding : Modal.options.padding;
            
            // //width
             Modal.options.width = options.width != undefined ? options.width : Modal.options.width;
             
             Modal.options.autoSize = options.width != undefined ? false : Modal.options.autoSize;

            //margin
            Modal.options.margin = options.margin != undefined ? options.margin : Modal.options.margin;

            //overlay
            Modal.options.helpers.overlay.closeClick = options.closeClick != undefined ? options.closeClick : Modal.options.helpers.overlay.closeClick;

            //closeBtn
            Modal.options.closeBtn = options.closeBtn != undefined ? options.closeBtn : Modal.options.closeBtn;


        };

    Modal.launchStaticContent = function(content, options){

            if(typeof(options) != 'undefined'){
                Modal.configureOptions(options);
            } 
            Modal.fancybox = $.fancybox;
            Modal.fancybox.open(
            {
                type: 'html',
                content: content
            }, 
            Modal.options
            );

            window.fancybox = Modal.fancybox;

            return Modal.fancybox;

        };

    Modal.launchStaticUrl =  function(url, data, options){
            if(typeof(options) != 'undefined'){                            
                Modal.configureOptions(options);
            } 

            Modal.fancybox = $.fancybox;
            Modal.fancybox.open(
                {
                    href: url,
                    type: 'ajax',
                }, 
                Modal.options
            );    

            window.fancybox = Modal.fancybox;    
            return Modal.fancybox;
        };

    Modal.launchConfirm = function(content, callback, options, arguments){
        var confirm = '<div id="modal-confirm-container"> <label class="fancybox-header"> Confirm </label> <p>' + content + '</p> <p style="float:right"><button type="submit" class="btn btn-primary" data-action="modal-confirm">Confirm</button> <button type="submit" class="btn btn-default" data-action="modal-cancel" >Cancel</button></p> <div style="clear:both"></div></div>';

        $('body').off('click', '#modal-confirm-container [data-action="modal-confirm"]');
        $('body').on('click', '#modal-confirm-container [data-action="modal-confirm"]', function(e){           
            Modal.close();
            if(callback != undefined){
                if(arguments != undefined){
                    callback.apply(null, arguments);
                }
                else{
                    callback();   
                }
            }
        }.bind(callback, arguments));

        return Modal.launchStaticContent(confirm, options);
    };

    Modal.close = function(modal){
        if(modal != undefined){
            modal.close();
        }

        if(window.fancybox != undefined && window.fancybox != null){
            window.fancybox.close();
        }

        if(Modal.fancybox != undefined && Modal.fancybox != null){
            Modal.fancybox.close();
        }

    };

    Modal.launchAlert = function(content, timeout){
        if(timeout == undefined){
            timeout = 1800;
        }

        content = '<div style="text-align:center; white-space:nowrap"> <h3> ' + content + ' </h3> </div>';

        Modal.launchStaticContent(content);
        if (timeout > 0) {
            window.setTimeout(function(){ Modal.close(); }, timeout);
        }
    }

    Modal.resize = function(modal){    
        if(modal != undefined){
            modal.toggle();
        }

        if(window.fancybox != undefined && window.fancybox != null){
            window.fancybox.toggle();
        }

        if(Modal.fancybox != undefined && Modal.fancybox != null){
            Modal.fancybox.toggle();
        }    
    }


    Modal.init = function(){
        $('body').on('click', '#modal-confirm-container [data-action="modal-cancel"]', function(e){
            Modal.close();
        });
    }

    Modal.init();

    return Modal;

}());



$(document).ready(function(){
    $('.mobile-hamburger').on('click', function(e){
        // var left_nav_html = $('.desktop-left-column').html();
        // $('.mobile-left-column').append(left_nav_html);

        $('.main-row .left-side ul, .desktop-left-column ul').toggleClass('un-hide');
    })    


})